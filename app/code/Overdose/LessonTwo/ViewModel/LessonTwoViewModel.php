<?php
namespace Overdose\LessonTwo\ViewModel;

class LessonTwoViewModel implements \Magento\Framework\View\Element\Block\ArgumentInterface
{
    public function show()
    {
        return "Test ViewModel (from ViewModel)" . "<br>";
    }
}
