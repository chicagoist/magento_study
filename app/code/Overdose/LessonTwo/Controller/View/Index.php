<?php
namespace Overdose\LessonTwo\Controller\View;

use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Result\Page;

class Index extends \Magento\Framework\App\Action\Action
{
    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
       /** @var Page $page */
       $page = $this->resultFactory->create(ResultFactory::TYPE_PAGE);

       /** @var Template $block */
       $block = $page->getLayout()->getBlock('overdose.lessontwo.layout.default');
       $page->getConfig()->getTitle()->prepend(__('Из контроллера View в шаблон default.phtml'));

       return $page;
    }
}
