<?php
namespace Overdose\LessonTwo\Block;

use \Magento\Customer\Model\Session;

class Lesson extends \Magento\Framework\View\Element\Template
{
    public function show()
    {
        return 'Test block from BLOCK(Block/Lesson.php)' . "<br>";
    }
}
