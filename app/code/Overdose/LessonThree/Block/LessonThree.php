<?php
namespace Overdose\LessonThree\Block;

use \Magento\Customer\Model\Session;

class LessonThree extends \Magento\Framework\View\Element\Template
{
    public function show()
    {
        return 'Test block from BLOCK(Block/Lesson.php)' . "<br>";
    }
}
