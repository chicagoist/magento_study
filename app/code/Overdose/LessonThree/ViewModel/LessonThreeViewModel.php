<?php
namespace Overdose\LessonThree\ViewModel;

class LessonThreeViewModel implements \Magento\Framework\View\Element\Block\ArgumentInterface
{
    protected $lessonthreeFactory;
    protected $lessonthreeResourceModel;
    protected $lessonthreeCollectionFactory;

    private $model = null;

    public function __construct(
        \Overdose\LessonThree\Model\LessonthreeFactory $lessonthreeFactory,
        \Overdose\LessonThree\Model\ResourceModel\Lessonthree $lessonthreeResourceModel,
        \Overdose\LessonThree\Model\ResourceModel\Collection\LessonthreeFactory $lessonthreeCollectionFactory
    ) {
        $this->lessonthreeFactory = $lessonthreeFactory;
        $this->lessonthreeResourceModel = $lessonthreeResourceModel;
        $this->lessonthreeCollectionFactory = $lessonthreeCollectionFactory;
    }

    public function show()
    {
        return "Declarative Schema (from ViewModel)" . "<br>";
    }

    public function createNewColumn($model, $year_of_produce, $comment)
    {
        for ($i = 0; $i < 10; $i++) {
            $model = $this->lessonthreeFactory->create();

            $model->setData('model', $model)
                ->setData('year_of_produce', $year_of_produce)
                ->setData('comment', $comment);

            $this->lessonthreeResourceModel->save($model);
        }
    }

    public function showAllCars()
    {
        $collection = $this->lessonthreeCollectionFactory->create();

        $collection->addFieldToFilter('id', ['eq' => 3]);

        $collection->load();

        return $collection->getItems();
    }

    public function getModelName($id)
    {
        return $this->getNameModel($id)->getData('model');
    }

    private function getNameModel($id)
    {
        if ($this->model === null) {
            $model = $this->lessonthreeFactory->create();

            $this->lessonthreeResourceModel->load($model, $id);

            $this->model = $model;
        }

        return $this->model;
    }
}
