<?php
namespace Overdose\LessonThree\Model\ResourceModel;

class Lessonthree extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{
    protected function _construct()
    {
        $this->_init('overdose_lesson_three', 'id');
    }
}
