<?php
namespace Overdose\LessonThree\Model\ResourceModel\Collection;

class Lessonthree extends \Magento\Framework\Model\ResourceModel\Db\Collection\AbstractCollection
{
    protected function _construct()
    {
        $this->_init(
            \Overdose\LessonThree\Model\Lessonthree::class,
            \Overdose\LessonOne\Model\ResourceModel\Lessonthree::class
        );
    }
}
