<?php
namespace Overdose\LessonThree\Model;

class Lessonthree extends \Magento\Framework\Model\AbstractModel
{
    protected function _construct()
    {
        $this->_init(\Overdose\LessonThree\Model\ResourceModel\Lessonthree::class);
    }
}
