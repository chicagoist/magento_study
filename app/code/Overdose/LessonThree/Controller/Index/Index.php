<?php
namespace Overdose\LessonThree\Controller\Index;

use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\View\Element\Template;

class Index extends \Magento\Framework\App\Action\Action
{
    /**
     * @return \Magento\Framework\App\ResponseInterface|\Magento\Framework\Controller\ResultInterface
     */
    public function execute()
    {
        $page = $this->resultFactory->create(ResultFactory::TYPE_PAGE);

        /** @var Template $block */
        $block = $page->getLayout()->getBlock('overdose.lessonthree.block');
        $block->setData('model', 'BMW M5')
            ->setData('year_of_produce', '1974')
            ->setData('comment', 'GERMAN TRACTOR');

        $page->getConfig()->getTitle()->prepend(__("Lesson Three. Declarative Schema"));

        return $page;
    }
}
